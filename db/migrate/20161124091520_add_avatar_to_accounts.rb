class AddAvatarToAccounts < ActiveRecord::Migration[5.0]
  def up
  	add_attachment :accounts, :avatar
  end

  def down
  	remove_attachment :accounts, :avatar
  end
end
