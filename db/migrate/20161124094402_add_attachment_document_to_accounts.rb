class AddAttachmentDocumentToAccounts < ActiveRecord::Migration
  def self.up
    change_table :accounts do |t|
      t.attachment :document
    end
  end

  def self.down
    remove_attachment :accounts, :document
  end
end
