Rails.application.routes.draw do
  devise_for :accounts, controllers: {registrations: 'registrations'}
  root 'accounts#index'
  resources :accounts do
  	put 'doc', action: 'change_document', on: :member
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
