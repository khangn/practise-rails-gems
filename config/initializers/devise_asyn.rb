module Devise
	module Models
		module Confirmable
			handle_asynchronously :send_confirmation_instructions
		end
	end
end