class AccountsController < ApplicationController
	before_action :authenticate_account!
  def index
  	
  end

  def show
  	@account = Account.find params[:id]
  	@posts = @account.posts.page params[:page]
  end

  def edit
  	@account = Account.find params[:id]
  end

  def update
  	@account = Account.find params[:id]
  	if @account.update account_params
  		redirect_to @account
  	else
  		render 'edit'
  	end
  end

  def change_document
  	update
  end

  private
  def account_params
  	params.require(:account).permit(:document,:nickname,:first_name,:last_name,:email,:avatar)
  end
end
